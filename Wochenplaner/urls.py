from django.conf.urls import patterns, include, url
from django.contrib import admin

from weekplan import views

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^weekplan/', include('weekplan.urls', namespace='weekplan')),
)
