from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse

from weekplan.models import Appointment, Event

from datetime import datetime

def index(request):
    return HttpResponse('index')

table_head = ['', 'Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag', 'Sonntag']

days = ['Montag', 'Dienstag', 'Mittwoch', 'Donnerstag', 'Freitag', 'Samstag', 'Sonntag']
hours = range(7, 21)


def week(request):
    table_body = [[None]*7 for i in range(14)]

    appointments = Appointment.objects.all()
    now = datetime.now()
    now_hour = now.hour
    now_day = now.weekday() + 1

    next_app = None

    for entry in appointments:
        x = entry.hour - 7
        y = entry.day - 1

        if now_day == entry.day and ((not next_app) or next_app.hour > entry.hour >= now_hour):
            next_app = entry

        if 0 <= x <= 14 and 0 <= y <= 7:
            table_body[x][y] = entry.event.name

    info_text = 'Anstehend: "%s" um %d:00 Uhr' % (next_app.event.name, next_app.hour) if next_app else 'keine Termine'

    return render(request, 'weekplan/week.html', {'table_body': table_body, 'table_head': table_head,
                                                  'next_appointment': info_text})


def submit(request, day, hour):
    if 'cancel' in request.POST:
        return HttpResponseRedirect(reverse('weekplan:week'))

    if not (0 < int(day) < 8 or 6 < int(hour) < 21):
        return HttpResponse('invalid date')

    name = request.POST['name']
    description = request.POST['description']
    selected_hour = request.POST['hour']
    selected_day = request.POST['day']
    delete = 'delete' in request.POST

    if not (0 < int(selected_day) < 8 or 6 < int(selected_hour) < 21):
        return HttpResponse('invalid date')

    if delete:
        try:
            app = Appointment.objects.get(day=day, hour=hour)
            event = app.event
            app.delete()
            # löst DoesNotExist exception aus falls keine zum event zugehörigen appointments existieren
            Appointment.objects.get(event=event)
        except Appointment.DoesNotExist:
            event.delete()  # falls es keine zugehörigen Appointments gibt, löschen.
        except Appointment.MultipleObjectsReturned:
            pass

    else:
        try:
            app = Appointment.objects.get(hour=hour, day=day)
            event = app.event

            # update event falls geändert
            event.name = name
            event.description = description
            event.save()

            # update appointment
            app.hour = selected_hour
            app.day = selected_day
            app.save()
        except Appointment.DoesNotExist:
            # falls an dieser Stelle noch kein Appointment existiert, neues Appointment
            try:
                event = Event.objects.get(name=name)
            except Event.DoesNotExist:
                # falls es kein Event mit dem gewünschtem Namen gibt, neues erstellen
                event = Event(name=name, description=description)
                event.save()
            Appointment(day=selected_day, hour=selected_hour, event=event).save()
        except Appointment.MultipleObjectsReturned:
            return HttpResponse('database corrupt? BUT HOW?!?!?!??')

    # zum Wochenplaner zurücklinken
    return HttpResponseRedirect(reverse('weekplan:week'))


def appointment(request, day, hour):

    try:
        apptmnt = Appointment.objects.get(day=day, hour=hour)
        # ein appointment an dieser Stelle existiert bereits, wir fügen also die entsprechenden daten ein.
        return render(request, 'weekplan/appointment.html',
                      {'appointment':apptmnt, 'event': apptmnt.event, 'days': days, 'hours': hours, 'selected_day': int(day), 'selected_hour': int(hour)})
    except Appointment.DoesNotExist:
        pass  # ein appointment an dieser Stelle exisitiert noch nicht, ein neues wird erstellt
    except Appointment.MultipleObjectsReturned:
        return HttpResponse('database corrupt? BUT HOW?!?!?!??')

    return render(request, 'weekplan/appointment.html', {'days': days, 'hours': hours, 'selected_day': int(day), 'selected_hour': int(hour)})

