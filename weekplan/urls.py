from django.conf.urls import patterns, url

from weekplan import views

urlpatterns = patterns('',
    # /weekplan/
    url(r'^$', views.index, name='index'),
    # /weekplan/week/
    url(r'^week/$', views.week, name='week'),
    # /weekplan/week/<day>/<hour>/appointment/...
    url(r'^week/appointment/(?P<day>\d+)/(?P<hour>\d+)/$', views.appointment),
    # /weekplan/week/<day>/<hour>/register/...
    url(r'^week/appointment/(?P<day>\d+)/(?P<hour>\d+)/submit/$', views.submit),
)