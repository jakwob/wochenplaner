from django.db import models


class Event(models.Model):
    name = models.CharField(max_length=100)
    description = models.CharField(max_length=200)


class Appointment(models.Model):
    day = models.IntegerField()
    hour = models.IntegerField()
    event = models.ForeignKey(Event)